from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json


# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "color",
        "brand",
        "size",
        "price",
        "picture",
        "id",

    ]

    def get_extra_data(self, o):
         return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "color",
        "brand",
        "size",
        "price",
        "picture",
        "id",
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:

            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid bin id"},
                status=400,
            )


        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid id"},
                status=400,
            )
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)

        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid bin id"},
                status=400,
            )

        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})
