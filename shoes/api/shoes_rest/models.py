from django.db import models

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.IntegerField()
    bin_size = models.IntegerField()




class Shoe(models.Model):
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    brand = models.CharField(max_length=100)
    size = models.FloatField()
    price = models.FloatField()
    picture = models.URLField(null=True)

    bin = models.ForeignKey(BinVO, related_name="shoes", on_delete=models.CASCADE, null=True)


    def __str__(self):
        return self.name
