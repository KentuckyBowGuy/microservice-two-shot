from django.contrib import admin
from .models import LocationVO, Hat

# Register your models here.
@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "fabric",
        "style_name",
        "location",
    ]

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "import_href",
    ]
