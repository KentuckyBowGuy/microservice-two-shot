from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import LocationVO, Hat
import json


# Encoders
class LocationVOEncoder(ModelEncoder):
    model=LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }



class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location"
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


# View to Get Hats (all locaitons or specific) and to Create Hat
@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        # handle case where we want all hats in all locations
        if not location_vo_id:
            hats = Hat.objects.all()
        # then handle case where we want all hats in a specified location
        else:
            hats = Hat.objects.filter(location=location_vo_id)
        # then return the list
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else: #POST
        # load the data from the request into content
        content = json.loads(request.body)
        print(content)

        # then try to pull a location
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400,
            )

        # finally, create a hat based on the provided content
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        print(content)
        # handle the case where the location is being updated
        try:
            if "location" in content:
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        # update the hat object and return the hat details
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else: #DELETE
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
