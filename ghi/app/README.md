# Wardrobify

Team:

* Trevor M - Shoes
* Person 2 - Which microservice?

## How to Run this App
1. Fork and clone this repository

2. Build and run the repository onto your computer with the following commands:
    - docker volume create two-shot-pgdata
    - docker-compose build
    - docker-compose up

3. Ensure the containers are running in Docker Desktop

4. View the react application in your browser at http://localhost:3000/


## Diagram
 - Put diagram here

## API Documentation

### URLs and Ports
 Ports:
    - 3000 - React Application
    - 8080:8000 - Shoes API
    - 8090:8000 - Hats API
    - 8100:8000 - Wardrobe API
    - 15432:5432 - Postgres Database

URLs:
    - http://localhost:3000 - React Application
    - http://localhost:8080 - Shoes API
    - http://localhost:8090 - Hats API
    - http://localhost:8100 - Wardrobe API
    - http://localhost:15432 - Postgres Database

### Shoe API
 -This API is responsible for the management of shoes in the database. It is a RESTful API that allows for the creation, reading, updating, and deletion of shoes. The API is built using Python and Django. The API also runs synonymously with a poller that is designed to poll information from the wardrobe microservice

### Hats API
 - Put Hats API documentation here

## Value Objects
 - BinVO - a value object that is used to link the bins to the shoes. It contains the following fields:
    - import_href - the unique url of the import
    - bin_number - the id of the bin
    - bin_size  - the id of the shoe
