import React, { useEffect, useState} from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [brand, setBrand] = useState('');
    const [size, setSize] = useState('');
    const [price, setPrice] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.color = color;
        data.brand = brand;
        data.size = size;
        data.price = price;
        data.picture = picture;
        data.bin = event.target.bin.value;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setName('');
            setColor('');
            setBrand('');
            setSize('');
            setPrice('');
            setPicture('');
            setBin('');
        } else if (response.status === 400) {
            const error = await response.json();
            console.log(error);
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleBrandChange = (event) => {
        const value = event.target.value;
        setBrand(value);
    }

    const handleSizeChange = (event) => {
        const value = event.target.value;
        setSize(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    const fetchData = async () => {
        const binUrl = 'http://localhost:8100/api/bins/';
        const response = await fetch(binUrl);
        if (response.ok) {
            const binData = await response.json();
            console.log(binData);
            setBins(binData.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new pair of shoes!</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange = {handleNameChange} value = {name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Shoe name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleColorChange} value = {color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleBrandChange} value = {brand} placeholder="Brand" required type="text" name="brand" id="brand" className="form-control" />
                            <label htmlFor="brand">Brand</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleSizeChange} value = {size} placeholder="Size" required type="float" name="size" id="size" className="form-control" />
                            <label htmlFor="size">Size</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handlePriceChange} value = {price} placeholder="Price" type="float" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handlePictureChange} value = {picture} placeholder="Picture" type="url" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">Picture</label>
                        </div>
                        <div className="mb-3">
                            <select onChange = {handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option >Please select a bin</option>
                                {bins.map(bin => (
                                    <option key={bin.href} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-dark">Add to closet!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
