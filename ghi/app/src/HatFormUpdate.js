import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

export default function HatFormUpdate() {
    const { hatId } = useParams();
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        "fabric": "",
        "style_name": "",
        "color": "",
        "picture_url": "",
        "location": ""
    })

    const fetchFormData = async () => {
        const hatsDetailApiUrl = `http://localhost:8090/api/hats/${hatId}/`;
        const response = await fetch(hatsDetailApiUrl);
        if (response.ok) {
            const data = await response.json();
            if (data !== undefined) {
                const hat = data.hat;
                setFormData({
                    "fabric": hat.fabric,
                    "style_name": hat.style_name,
                    "color": hat.color,
                    "picture_url": hat.picture_url,
                    "location": hat.location.import_href
                })
            }
            else {
                return null;
            }
        }
        else {
            console.error(response);
        }
    }

    const fetchLocations = async () => {
        const locationsApiUrl = "http://localhost:8100/api/locations/";

        const response = await fetch(locationsApiUrl);
        if (response.ok) {
            const data = await response.json();
            if (data === undefined) {
                return null;
            }
            setLocations(data.locations);
        }
        else {
            console.error(response);
        }
    };


    useEffect(() => {
        fetchFormData();
        fetchLocations();
    }, []);


    const handleFormChange = (e) => {
        const value = e.target.value;
        const formName = e.target.name;

        setFormData({
            ...formData,
            [formName]: value
        })
    }


    const handleSubmit = async (e) => {
        e.preventDefault();

        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(formData),
            headers: {"Content-Type": "application/json"},
        };

        const hatsDetailApiUrl = `http://localhost:8090/api/hats/${hatId}/`;
        const response = await fetch(hatsDetailApiUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                "fabric": "",
                "style_name": "",
                "color": "",
                "picture_url": "",
                "location": ""
            });
        }
        else {
            console.error(response);
        }
    }

    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new hat!</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.style_name} placeholder="Style" required type="text" id="style_name" name="style_name" className="form-control" />
                            <label htmlFor="style_name">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture" required type="text" id="picture_url" name="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.location} required id="location" name="location" className="form-select">
                                <option defaultValue value="">Choose a location</option>
                                {locations.map( location => {
                                    return (
                                        <option key={location.id} value={location.href}>
                                            {location.closet_name} - {location.section_number}/{location.shelf_number}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
