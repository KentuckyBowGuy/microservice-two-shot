import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

export default function HatList() {
    const [hats, setHats] = useState([]);
    const [selectedHat, setSelectedHat] = useState(null);

    const fetchHats = async () => {
        const hatsApiUrl = "http://localhost:8090/api/hats/";
        const response = await fetch(hatsApiUrl);
        if (response.ok) {
            const data = await response.json();

            if (data === undefined) {
                return null;
            }

            setHats(data.hats);
        }
        else {
            console.error(response);
        }
    }

    useEffect( () => {
        fetchHats();
    }, []);


    const handleHatClick = (hat) => {
        if (selectedHat === hat) {
            setSelectedHat(null);
        }
        else{
            setSelectedHat(hat);
        }
    }


    const handleDelete = async () => {
        const deleteHatsApiUrl = `http://localhost:8090/api/hats/${selectedHat.id}`;
        const fetchConfig = {
            method: "DELETE"
        }

        const response = await fetch(deleteHatsApiUrl, fetchConfig);
        fetchHats();
        setSelectedHat(null);
    }

    return (
        <div>
            <table className="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Location</th>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr
                                key={hat.id}
                                className={ selectedHat === hat ? "table-primary" : "" }
                                onClick={ () => handleHatClick(hat)}
                            >
                                <td>{hat.id}</td>
                                <td>{hat.location.closet_name} — {hat.location.section_number}/{hat.location.shelf_number}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{hat.color}</td>
                                <td>
                                    <img className="list-image" src={hat.picture_url} alt="" />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            {selectedHat && (
                <div>
                    <h2>Hat Details:</h2>
                    <p>Fabric: {selectedHat.fabric}</p>
                    <p>Style: {selectedHat.style_name}</p>
                    <p>Color: {selectedHat.color}</p>
                    <p>Closet: {selectedHat.location.closet_name}</p>
                    <p>Section: {selectedHat.location.section_number}</p>
                    <p>Shelf: {selectedHat.location.shelf_number}</p>
                    <button onClick={handleDelete} className="btn border">Delete Hat</button>
                    <button className="btn border m-0 p-0"><NavLink className="nav-link" to={`/hats/${selectedHat.id}/update/`}>Update Hat</NavLink></button>
                </div>
            )}
        </div>
    )
}
