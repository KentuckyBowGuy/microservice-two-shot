import React, {useState} from 'react';

function BinForm(props) {
    const [closetName, setClosetName] = useState('');
    const [binNumber, setBinNumber] = useState('');
    const [binSize, setbinSize] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.closet_name = closetName;
        data.bin_number = binNumber;
        data.bin_size = binSize;
        console.log(data);


        const binUrl = 'http://localhost:8100/api/bins/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            setClosetName('');
            setBinNumber('');
            setbinSize('');
        }
    }

    const handleClosetNameChange = (event) => {
        const value = event.target.value;
        setClosetName(value);
    }

    const handleBinNumberChange = (event) => {
        const value = event.target.value;
        setBinNumber(value);
    }

    const handleBinSizeChange = (event) => {
        const value = event.target.value;
        setbinSize(value);
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new bin</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
                        <div className="form-floating mb-3">
                            <input onChange = {handleClosetNameChange} value = {closetName} placeholder="Closet name" required type="text" name="closet_name" id="closet_name" className="form-control" />
                            <label htmlFor="closet_name">Closet name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleBinNumberChange} value = {binNumber} placeholder="Bin number" required type="number" name="bin_number" id="bin_number" className="form-control" />
                            <label htmlFor="bin_number">Bin number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleBinSizeChange} value = {binSize} placeholder="Bin size" required type="text" name="bin_size" id="bin_size" className="form-control" />
                            <label htmlFor="bin_size">Bin size</label>
                        </div>
                        <div className="mb-3">
                        </div>
                        <button className="btn btn-dark">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default BinForm;
