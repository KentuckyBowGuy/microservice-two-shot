import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import BinForm from './BinForm';
import HatList from './HatList';
import HatForm from './HatForm';
import HatFormUpdate from './HatFormUpdate';
import LocationForm from './LocationForm';
import ShoeUpdate from './ShoeUpdate';


function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/bins/new" element={<BinForm />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/locations/new" element={<LocationForm />}/>
          <Route path="/shoes/:id" element={<ShoeUpdate />} />
          <Route path="hats" element={<HatList />}/>
          <Route path="hats/new" element={<HatForm />}/>
          <Route path="hats/:hatId/update" element={<HatFormUpdate />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
