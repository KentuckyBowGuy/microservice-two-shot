import React, { useState, useEffect} from "react";

export default function LocationForm() {
    const [formData, setFormData] = useState({
        "closet_name": "",
        "section_number": "",
        "shelf_number": ""
    })


    const handleFormChange = (e) => {
        const value = e.target.value;
        const formName = e.target.name

        setFormData({
            ...formData,
            [formName]: value
        })
    }


    const handleSubmit = async (e) => {
        e.preventDefault();

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {"Content-Type": "applicaton/json"},
        };

        const locationsApiUrl = `http://localhost:8100/api/locations/`;
        const response = await fetch(locationsApiUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                "closet_name": "",
                "section_number": "",
                "shelf_number": ""
            })
        }
        else {
            console.log(response);
        }
    }


    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New location</h1>
                    <h2>To store your many hats</h2>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input
                                required
                                id="closet_name"
                                name="closet_name"
                                placeholder="Closet Name"
                                type="text"
                                className="form-control"
                                onChange={handleFormChange}
                                value={formData.closet_name}
                            />
                            <label htmlFor="closet_name">Closet Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                required
                                id="section_number"
                                name="section_number"
                                placeholder="Section Number"
                                type="number"
                                min="0"
                                step="1"
                                className="form-control"
                                onChange={handleFormChange}
                                value={formData.section_number}
                            />
                            <label htmlFor="section_number">Section Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                required
                                id="shelf_number"
                                name="shelf_number"
                                placeholder="Shelf Number"
                                type="number"
                                className="form-control"
                                onChange={handleFormChange}
                                value={formData.shelf_number}
                            />
                            <label htmlFor="shelf_number">Shelf Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
