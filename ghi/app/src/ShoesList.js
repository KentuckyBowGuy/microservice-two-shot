import React, { useEffect, useState } from "react";
function ShowShoe() {

    const [shoes, setShoes] = useState([]);
    const [selectedShoe, setSelectedShoe] = useState(null);

    const getShoes = async () => {
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const response = await fetch(shoesUrl);
        if (response.ok) {
            const shoeData = await response.json();

            if (shoeData === undefined) {
                return null;
            }

            setShoes(shoeData.shoes);
        } else {
            console.log('error');
        }
    }

    useEffect(() => {
        getShoes();
    }, []);

    const handleShoeClick = (shoe) => {
        setSelectedShoe(shoe);
    }

    const handleResetClick = async () => {
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const response = await fetch(shoesUrl);
        if (response.ok) {
            const shoeData = await response.json();

            if (shoeData === undefined) {
                return null;
            }
            setShoes(shoeData.shoes);
            setSelectedShoe(null);
        }
    }

    const handleDeleteClick = async () => {
        if (selectedShoe) {
            const deleteUrl = `http://localhost:8080/api/shoes/${selectedShoe.id}`;
            const response = await fetch(deleteUrl, {
                method: 'DELETE'
            });
            const shoesUrl = 'http://localhost:8080/api/shoes/';
            const responseTwo = await fetch(shoesUrl);
            if (response.ok) {
                const shoeData = await responseTwo.json();

                setShoes(shoeData.shoes);
                setSelectedShoe(null);
            }
        }
    }

    const handleUpdateClick = async () => {
        if (selectedShoe) {
          window.location.href = `/shoes/${selectedShoe.id}`;
        }
      }


    return (
        <div>
            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">Name</th>
                        <th className="fs-3" scope="col">Brand</th>
                        <th className="fs-3" scope="col">Color</th>
                        <th className="fs-3" scope="col">Size</th>
                        <th className="fs-3" scope="col">Bin</th>
                        <th className="fs-3" scope="col">Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes && shoes.map(shoe => (
                        <tr
                            className={selectedShoe === shoe ? "table-primary" : ""}
                            key={shoe.id}
                            onClick={() => handleShoeClick(shoe)}
                        >
                            <td className="fw-bold">{shoe.name}</td>
                            <td>{shoe.brand}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.size}</td>
                            <td>{shoe.bin}</td>
                            <td>
                                <img className="list-image" src={shoe.picture} alt=""/>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {selectedShoe && (
                <div>
                    <h2>Selected Shoe Details:</h2>
                    <p>Name: {selectedShoe.name}</p>
                    <p>Brand: {selectedShoe.brand}</p>
                    <p>Color: {selectedShoe.color}</p>
                    <p>Size: {selectedShoe.size}</p>
                    <p>Price: ${selectedShoe.price}</p>
                    <p>Bin: {selectedShoe.bin}</p>
                    <img className="list-image" src={selectedShoe.picture} alt=""/>
                    <div/>
                    <div className="d-flex justify-content-start gap-2 ">

                    <button className="btn btn-danger " onClick={handleDeleteClick}>Delete Shoe</button>
                    <button className="btn btn-warning" onClick={handleUpdateClick}>Update a shoe</button>
                    <button className="btn btn-primary" onClick={handleResetClick}>Reset</button>


                    </div>
                </div>
            )}
        </div>
    );
}

export default ShowShoe;
